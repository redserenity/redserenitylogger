<?php
$rustart = getrusage();
function rutime($ru, $rus, $index) {
	return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
				 -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
}


$Logger = new RedSerenity\Log\Logger("MyChannel", "America/Denver");
$Logger->AddHandler(new RedSerenity\Log\Handler\File("./log.log"));
$Logger->AddHandler(new RedSerenity\Log\Handler\Console());
$Logger->Notice("Hello, World!", ["Hi" => "There"]);
$Logger->Info("Hello, Universe!", ["Hi" => "Where"]);
$Logger->Error("Hello, Galaxy!", ["Hi" => "Where"]);

$ru = getrusage();
echo "This process used " . rutime($ru, $rustart, "utime") .
		 " ms for its computations\n";
echo "It spent " . rutime($ru, $rustart, "stime") .
		 " ms in system calls\n";