<?php

namespace RedSerenity\Log\Formatter;


interface FormatterInterface
{

    /**
     * @param \RedSerenity\Log\Record $LogRecord
     */
    public function Format(\RedSerenity\Log\Record $LogRecord);

}
