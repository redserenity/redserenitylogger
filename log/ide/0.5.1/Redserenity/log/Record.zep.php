<?php

namespace RedSerenity\Log;


class Record
{

    protected $TimeZone = null;


    protected $Message = null;


    protected $Context = array();


    protected $Level = Logger::ERROR;


    protected $LevelName = "ERROR";


    protected $Channel = null;


    protected $DateTime = null;


    protected $Extra = array();


    /**
     * @param \DateTimeZone $TimeZone
     * @param string $Channel
     * @param string $Message
     * @param array $Context
     * @param int $LogLevel
     * @param array $Extra
     */
    public function __construct(\DateTimeZone $TimeZone, $Channel, $Message = null, array $Context = null, $LogLevel = null, array $Extra = null) {}

    /**
     * @param string $Message
     * @return string
     */
    public function Message($Message = null) {}

    /**
     * @param array $Context
     * @return array
     */
    public function Context(array $Context = null) {}

    /**
     * @param int $LogLevel
     * @return int
     */
    public function Level($LogLevel = null) {}

    /**
     * @return string
     */
    public function LevelName() {}

    /**
     * @return string
     */
    public function Channel() {}

    /**
     * @return \DateTime
     */
    public function DateTime() {}

    /**
     * @param array $Extra
     * @return array
     */
    public function Extra(array $Extra = null) {}

    /**
     * @param \RedSerenity\Log\Formatter\FormatterInterface $Formatter
     */
    public function Render(\RedSerenity\Log\Formatter\FormatterInterface $Formatter = null) {}

}
