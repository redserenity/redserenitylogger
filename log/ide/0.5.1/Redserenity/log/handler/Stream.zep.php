<?php

namespace RedSerenity\Log\Handler;


class Stream extends \RedSerenity\Log\Handler\AbstractHandler
{

    protected $MinLevel = Logger::DEBUG;


    protected $StreamHandler = null;


    /**
     * @param string $Stream
     * @param int $LogLevel
     */
    public function __construct($Stream, $LogLevel = null) {}


    public function __destruct() {}

    /**
     * @param \RedSerenity\Log\Record $LogRecord
     * @return bool
     */
    public function Handle(\RedSerenity\Log\Record $LogRecord) {}

}
