<?php

namespace RedSerenity\Log\Handler;


interface HandlerInterface
{

    /**
     * @param \RedSerenity\Log\Record $LogRecord
     * @return bool
     */
    public function WillHandle(\RedSerenity\Log\Record $LogRecord);

    /**
     * @param \RedSerenity\Log\Record $LogRecord
     * @return bool
     */
    public function Handle(\RedSerenity\Log\Record $LogRecord);

    /**
     * @param array $LogRecords
     * @return bool
     */
    public function HandleBatch(array $LogRecords);

    /**
     * @param \RedSerenity\Log\Formatter\FormatterInterface $Formatter
     */
    public function SetFormatter(\RedSerenity\Log\Formatter\FormatterInterface $Formatter);

    /**
     * @return \RedSerenity\Log\Formatter\FormatterInterface
     */
    public function GetFormatter();

}
