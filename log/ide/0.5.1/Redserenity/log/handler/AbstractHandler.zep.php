<?php

namespace RedSerenity\Log\Handler;


abstract class AbstractHandler implements \RedSerenity\Log\Handler\HandlerInterface
{

    protected $Formatter = null;


    protected $MinLevel = Logger::DEBUG;


    /**
     * @param int $LogLevel
     */
    public function __construct($LogLevel = null) {}

    /**
     * @param \RedSerenity\Log\Record $LogRecord
     * @return bool
     */
    public function WillHandle(\RedSerenity\Log\Record $LogRecord) {}

    /**
     * @param \RedSerenity\Log\Record $LogRecord
     * @return bool
     */
    abstract public function Handle(\RedSerenity\Log\Record $LogRecord);

    /**
     * @param array $LogRecords
     * @return bool
     */
    public function HandleBatch(array $LogRecords) {}

    /**
     * @param \RedSerenity\Log\Formatter\FormatterInterface $Formatter
     */
    public function SetFormatter(\RedSerenity\Log\Formatter\FormatterInterface $Formatter) {}

    /**
     * @return \RedSerenity\Log\Formatter\FormatterInterface
     */
    public function GetFormatter() {}

}
