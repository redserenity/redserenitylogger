<?php

namespace RedSerenity\Log\Handler;


class DevNull extends \RedSerenity\Log\Handler\AbstractHandler
{

    protected $MinLevel = Logger::DEBUG;


    /**
     * @param \RedSerenity\Log\Record $LogRecord
     * @return bool
     */
    public function Handle(\RedSerenity\Log\Record $LogRecord) {}

}
