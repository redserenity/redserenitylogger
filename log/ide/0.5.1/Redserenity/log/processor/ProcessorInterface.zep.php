<?php

namespace RedSerenity\Log\Processor;


interface ProcessorInterface
{

    /**
     * @param \RedSerenity\Log\Record $LogRecord
     */
    public function Process(\RedSerenity\Log\Record $LogRecord);

}
