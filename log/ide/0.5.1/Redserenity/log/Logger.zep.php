<?php

namespace RedSerenity\Log;


class Logger implements \RedSerenity\Psr\Log\LoggerInterface
{

    const DEBUG = 100;


    const INFO = 200;


    const NOTICE = 250;


    const WARNING = 300;


    const ERROR = 400;


    const CRITICAL = 500;


    const ALERT = 550;


    const EMERGENCY = 600;


    static protected $_TimeZone = null;


    protected $ChannelName = "DEFAULT";


    protected $Handlers = array();


    protected $Processors = array();


    /**
     * @param string $ChannelName
     * @param string $TimeZone
     */
    public function __construct($ChannelName, $TimeZone = null) {}

    /**
     * @param string $TimeZone
     */
    public function TimeZone($TimeZone = null) {}

    /**
     * @param string $ChannelName
     * @return string
     */
    public function Channel($ChannelName = null) {}

    /**
     * @param string $ChannelName
     * @return \RedSerenity\Psr\Log\LoggerInterface
     */
    public function CloneChannel($ChannelName) {}

    /**
     * @param \RedSerenity\Log\Handler\HandlerInterface $Handler
     */
    public function AddHandler(\RedSerenity\Log\Handler\HandlerInterface $Handler) {}

    /**
     * @param array $Handlers
     */
    public function SetHandlers(array $Handlers) {}

    /**
     * @return array
     */
    public function GetHandlers() {}

    /**
     * @param \RedSerenity\Log\Processor\ProcessorInterface $Processor
     */
    public function AddProcessor(\RedSerenity\Log\Processor\ProcessorInterface $Processor) {}

    /**
     * @param array $Processors
     */
    public function SetProcessors(array $Processors) {}

    /**
     * @return array
     */
    public function GetProcessors() {}

    /**
     * @param int $Level
     */
    static public function LevelName($Level) {}

    /**
     * @param mixed $message
     * @param array $context
     */
    public function Debug($message, array $context = array()) {}

    /**
     * @param mixed $message
     * @param array $context
     */
    public function Info($message, array $context = array()) {}

    /**
     * @param mixed $message
     * @param array $context
     */
    public function Notice($message, array $context = array()) {}

    /**
     * @param mixed $message
     * @param array $context
     */
    public function Warning($message, array $context = array()) {}

    /**
     * @param mixed $message
     * @param array $context
     */
    public function Error($message, array $context = array()) {}

    /**
     * @param mixed $message
     * @param array $context
     */
    public function Critical($message, array $context = array()) {}

    /**
     * @param mixed $message
     * @param array $context
     */
    public function Alert($message, array $context = array()) {}

    /**
     * @param mixed $message
     * @param array $context
     */
    public function Emergency($message, array $context = array()) {}

    /**
     * @param mixed $level
     * @param mixed $message
     * @param array $context
     */
    public function Log($level, $message, array $context = array()) {}

}
