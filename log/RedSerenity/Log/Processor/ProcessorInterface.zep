namespace RedSerenity\Log\Processor;

use RedSerenity\Log\Record;

interface ProcessorInterface {
	public function Process(<Record> LogRecord);
}