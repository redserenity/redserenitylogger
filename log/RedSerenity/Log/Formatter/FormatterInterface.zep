namespace RedSerenity\Log\Formatter;

use RedSerenity\Log\Record;

interface FormatterInterface {
	public function Format(<Record> LogRecord);
}