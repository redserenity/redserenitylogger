namespace RedSerenity\Log\Formatter;

use DateTime;

use RedSerenity\Log\Record;

class SimpleLine implements FormatterInterface {

	public function Format(<Record> LogRecord) {
		var Output;

		let Output = sprintf("[%s] %s.%s: %s %s %s\n",
			LogRecord->DateTime()->Format(DateTime::ISO8601),
			LogRecord->Channel(),
			LogRecord->LevelName(),
			LogRecord->Message(),
			json_encode(LogRecord->Context()),
			json_encode(LogRecord->Extra())
		);

		return Output;
	}

}