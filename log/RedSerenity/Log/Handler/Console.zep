namespace RedSerenity\Log\Handler;

class Console extends Stream {

	public function __construct() {
		parent::__construct("php://stdout");
	}

}