namespace RedSerenity\Log\Handler;

use RedSerenity\Log\Logger;
use RedSerenity\Log\Record;

class Stream extends AbstractHandler {

	protected MinLevel = Logger::DEBUG;

	protected StreamHandler = null;

	public function __construct(string Stream, int LogLevel = null) {
		parent::__construct(LogLevel);

		let this->StreamHandler = fopen(Stream, "a");
		if (!this->StreamHandler) {
			throw new Exception("Could not open stream '" . Stream . "'");
		}
	}

	public function __destruct() {
		if (this->StreamHandler) {
			fclose(this->StreamHandler);
		}
	}

	public function Handle(<Record> LogRecord) -> bool {
		var FormattedRecord;
		let FormattedRecord = LogRecord->Render(this->Formatter);
		fwrite(this->StreamHandler, FormattedRecord);

		return true;
	}

}