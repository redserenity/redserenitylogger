namespace RedSerenity\Log\Handler;

use RedSerenity\Log\Logger;
use RedSerenity\Log\Record;
use RedSerenity\Log\Formatter\FormatterInterface;

abstract class AbstractHandler implements HandlerInterface {

	protected Formatter = null;
	protected MinLevel = Logger::DEBUG;

	public function __construct(int LogLevel = null) {
		if (LogLevel) {
			let this->MinLevel = LogLevel;
		}
	}

	public function WillHandle(<Record> LogRecord) -> bool {
		return (LogRecord->Level() >= this->MinLevel);
	}

	abstract public function Handle(<Record> LogRecord) -> bool;

	public function HandleBatch(array LogRecords) -> bool {
		var Record;
		for Record in LogRecords {
			if (!this->Handle(Record)) {
				return false;
			}
		}
		return true;
	}

	public function SetFormatter(<FormatterInterface> Formatter) -> void {
		let this->Formatter = Formatter;
	}

	public function GetFormatter() -> <FormatterInterface> {
		return this->Formatter;
	}

}