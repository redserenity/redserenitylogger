namespace RedSerenity\Log\Handler;

use RedSerenity\Log\Record;
use RedSerenity\Log\Formatter\FormatterInterface;

interface HandlerInterface {

	public function WillHandle(<Record> LogRecord) -> bool;
	public function Handle(<Record> LogRecord) -> bool;
	public function HandleBatch(array LogRecords) -> bool;
	public function SetFormatter(<FormatterInterface> Formatter) -> void;
	public function GetFormatter() -> <FormatterInterface>;

}