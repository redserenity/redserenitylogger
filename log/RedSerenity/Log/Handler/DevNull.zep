namespace RedSerenity\Log\Handler;

use RedSerenity\Log\Logger;
use RedSerenity\Log\Record;

class DevNull extends AbstractHandler {

	protected MinLevel = Logger::DEBUG;

	public function Handle(<Record> LogRecord) -> bool {
		return true;
	}

}