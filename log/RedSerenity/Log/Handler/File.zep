namespace RedSerenity\Log\Handler;

use RedSerenity\Log\Logger;
use RedSerenity\Log\Record;

class File extends Stream {

	public function __construct(string FileName) {
		parent::__construct(FileName);
	}

}