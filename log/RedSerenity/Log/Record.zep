namespace RedSerenity\Log;

use DateTime;
use DateTimeZone;
use RedSerenity\Log\Formatter\FormatterInterface;
use RedSerenity\Log\Formatter\SimpleLine as SimpleLineFormatter;

class Record {

	protected TimeZone  = null;

	protected Message   = null;
	protected Context   = [];
	protected Level     = Logger::ERROR;
	protected LevelName = "ERROR";
	protected Channel   = null;
	protected DateTime  = null;
	protected Extra     = [];

	public function __construct(<DateTimeZone> TimeZone, string Channel, string Message = null, array Context = null, int LogLevel = null, array Extra = null) {
		let this->TimeZone = TimeZone;
		let this->Channel = Channel;
		this->Message(Message);
		this->Context(Context);
		this->Level(LogLevel);
		this->Extra(Extra);
	}

	public function Message(string Message = null) -> string {
		if (Message) {
			let this->Message = Message;
		}

		return this->Message;
	}

	public function Context(array Context = null) -> array {
		if (!empty Context) {
			let this->Context = Context;
		}

		return this->Context;
	}

	public function Level(int LogLevel = null) -> int {
		if (LogLevel) {
			let this->Level = LogLevel;
			let this->LevelName = Logger::LevelName(LogLevel);
		}

		return this->Level;
	}

	public function LevelName() -> string {
		return this->LevelName;
	}

	public function Channel() -> string {
		return this->Channel;
	}

	public function DateTime() -> <DateTime> {
		return new DateTime("now", this->TimeZone);
	}

	public function Extra(array Extra = null) -> array {
		if (!empty Extra) {
			let this->Extra = array_merge(this->Extra, Extra);
		}

		return this->Extra;
	}

	public function Render(<FormatterInterface> Formatter = null) {
		if (Formatter) {
			return Formatter->Format(this);
		}

		return (new SimpleLineFormatter())->Format(this);
	}

}