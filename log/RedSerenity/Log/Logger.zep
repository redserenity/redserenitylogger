namespace RedSerenity\Log;

use DateTimeZone;
use RedSerenity\Psr\Log\LoggerInterface;
use RedSerenity\Log\Handler\HandlerInterface;
use RedSerenity\Log\Processor\ProcessorInterface;

class Logger implements LoggerInterface {

	const DEBUG     = 100;
	const INFO      = 200;
	const NOTICE    = 250;
	const WARNING   = 300;
	const ERROR     = 400;
	const CRITICAL  = 500;
	const ALERT     = 550;
	const EMERGENCY = 600;

	static protected _TimeZone = null;

	protected ChannelName = "DEFAULT";
	protected Handlers    = [];
	protected Processors  = [];

	public function __construct(string ChannelName, string TimeZone = null) {
		let this->ChannelName = ChannelName;

		if !TimeZone {
			var DefaultTimeZone = date_default_timezone_get();
			let TimeZone = DefaultTimeZone ? DefaultTimeZone : "UTC";
		}
		this->TimeZone(TimeZone);
	}

	public function TimeZone(string TimeZone = null) {
		if TimeZone {
			// new \DateTimeZone(date_default_timezone_get() ?: 'UTC');
  		let self::_TimeZone = new DateTimeZone(TimeZone);
  	}

  	return self::_TimeZone;
	}

	public function Channel(string ChannelName = null) -> string {
		if (ChannelName) {
			let this->ChannelName = ChannelName;
		}

		return this->ChannelName;
	}

	public function CloneChannel(string ChannelName) -> <LoggerInterface> {
		var ClonedChannel;
		let ClonedChannel = clone this;
		ClonedChannel->Channel(ChannelName);
		return ClonedChannel;
	}

	public function AddHandler(<HandlerInterface> Handler) -> void {
		let this->Handlers[] = Handler;
	}

	public function SetHandlers(array Handlers) -> void {
		let this->Handlers = Handlers;
	}

	public function GetHandlers() -> array {
		return this->Handlers;
	}

	public function AddProcessor(<ProcessorInterface> Processor) -> void {
		let this->Processors[] = Processor;
	}

	public function SetProcessors(array Processors) -> void {
		let this->Processors = Processors;
	}

	public function GetProcessors() -> array {
		return this->Processors;
	}

	static public function LevelName(int Level) {
     switch Level {
     		case 100: return "DEBUG";
				case 200: return "INFO";
				case 250: return "NOTICE";
				case 300: return "WARNING";
				case 400: return "ERROR";
				case 500: return "CRITICAL";
				case 550: return "ALERT";
				case 600: return "EMERGENCY";
     }
	}

	public function Debug(message, array context = []) -> void {
		this->Log(self::DEBUG, message, context);
	}

	public function Info(message, array context = []) -> void {
		this->Log(self::INFO, message, context);
	}

	public function Notice(message, array context = []) -> void {
		this->Log(self::NOTICE, message, context);
	}

	public function Warning(message, array context = []) -> void {
		this->Log(self::WARNING, message, context);
	}

	public function Error(message, array context = []) -> void {
		this->Log(self::ERROR, message, context);
	}

	public function Critical(message, array context = []) -> void {
		this->Log(self::CRITICAL, message, context);
	}

	public function Alert(message, array context = []) -> void {
		this->Log(self::ALERT, message, context);
	}

	public function Emergency(message, array context = []) -> void {
		this->Log(self::EMERGENCY, message, context);
	}

	public function Log(level, message, array context = []) -> void {
		/* If no handlers are registered, there is nothing to do. */
		if empty this->Handlers { return; }

		var Handler, Processor, LogRecord;

		/* Create the Log Record from the raw log. */
		let LogRecord = new Record(self::_TimeZone, this->ChannelName, message, context, level);


		for Processor in this->Processors {
			Processor->Process(LogRecord);
		}

		for Handler in this->Handlers {
			if (Handler->WillHandle(LogRecord)) {
				if (!Handler->Handle(LogRecord)) {
					return;
				}
			}
		}

	}

}